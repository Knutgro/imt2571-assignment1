<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
	
    protected $db = null;  
    
/* Connects to database unless its already connected */
    public function __construct($db = null)  
    { 
	    if ($db) 
		{
			$this->db = $db;
		}
		else
		{
			try
			{                                                                         
				$this->db = new PDO('mysql:host=127.0.0.1;dbname=assignment1','root','');
				$this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch (PDOException $e)
			{
				echo 'connection failed:  '. $e->getMessage();                             // error appears if connection fails
			}
		}
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
	 * @throws PDOException
     */
    public function getBookList()
    {
		try
		{
			$stmt = $this->db->prepare('SELECT * FROM books ORDER BY id');
			$stmt->setFetchMode(PDO::FETCH_CLASS, 'Book');
			$stmt->execute();
			//$booklist = $stmt->fetchAll(PDO::FETCH_OBJ);
			$booklist = $stmt->fetchAll();
			return $booklist;  
		}
		catch (PDOException $e)
		{
			echo 'Failed to get BookList   '. $e->getMessage();
		}
    }
    
    /** Function retrieving information about a given book in the collection.
	 * 
     * @param integer $id the id of the book to be retrieved
     * @return Book|null The book matching the $id exists in the collection; null otherwise.
	 * @throws PDOException
     */
    public function getBookById($id)
    {
		
		if($stmt = $this->db->prepare("SELECT * FROM books WHERE id = '$id' ")){
			try
			{
				$stmt->setFetchMode(PDO::FETCH_CLASS,'Book');
				$stmt->bindParam(':id',$id);
				$stmt->execute();
				$book = $stmt->fetch(PDO::FETCH_OBJ);
			}
			catch (PDOException $e)
			{
				echo 'Failed to get book  '. $e->getMessage();
			}
		}
		else {
			$book = null;
		}
        return $book;
    }
    
    /** Adds a new book to the collection.
     * @param $book Book The book to be added - the id of the book will be set after successful insertion.
	 * @throws PDOException
     */
    public function addBook($book)
    {
		if (!empty($book->title) && !empty($book->author))
		{
			try
			{
				$query = "INSERT INTO books (title, author, description) VALUES (:title, :author, :description)";
				$stmt = $this->db->prepare($query);
				
				$stmt->bindParam(':title',$book->title);
				$stmt->bindParam(':author',$book->author);
				$stmt->bindParam(':description',$book->description);
				$inserted = $stmt->execute();
		
				if($inserted)
				{
					$book->id = $this->db->lastInsertId();
					echo 'Book inserted!<br>  ';
				}
			}
			catch(PDOException $e)
			{
				echo 'Failed to insert book!<br>  '. $e->getMessage();
			}
		}
		else
		{
			echo 'Title and Author fields may not be empty';
			die();
		}
	}

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
		if (!empty($book->title) && !empty($book->author) && !is_int($book->id))
		{
			try
			{
				$query = "UPDATE books SET title = :title , author = :author, description = :description WHERE id = :id ";
				$stmt = $this->db->prepare($query);
				$stmt->bindParam(':title',$book->title);
				$stmt->bindParam(':author',$book->author);
				$stmt->bindParam(':description',$book->description);
				$stmt->bindParam(':id',$book->id);
				$updated = $stmt->execute();
				
				if($updated)
				{
					echo 'Book updated!<br>  ';
				}
			}
			catch(PDOException $e)
			{
				echo 'Failed to update book!<br>  '. $e->getMessage();
			}
		}
		else
		{
			echo 'Title and Author fields may not be empty';
			die();
		}
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
		if(!is_int($id))
		{
			try
			{
				$query = "DELETE FROM books WHERE id = $id ";
				$stmt = $this->db->prepare($query);
				$stmt->bindParam(':id',$id);
				$deleted = $stmt->execute();
				if($deleted)
				{
					echo 'Book deleted!<br>  ';
				}
			}
			catch(PDOException $e)
			{
				echo 'Failed to delete book!<br>  '. $e->getMessage();
			}
		}
		else
		{
			echo 'Id is not an integer!';
			die();
		}
    }
}

?>